<?php 
/*
Pengulangan Pada Array
1. For
2. Foreach
*/
$angka=[123,23,3,23,4,11,"string"];
$mobil=["warna" =>"merah","tahun" =>"2019"];
?>

 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
 	<title>Manmpilkan Array dengan perulangan</title>
 	<style type="text/css">
 		.kotak{
 			width: 50px;
 			height: 50px;
 			background-color: salmon;
 			line-height: 50px;
 			text-align: center;
 			border-radius: 10px;
 			color:white;
 			float: left;
 			margin:3px;
 		}
 		.clear:{clear:both;}
 	</style>
 	<link rel="stylesheet" href="">
 </head>
 <body>
 	<h1>Menampilkan Array</h1>
 	<p>Menampmilkan dengan for</p>
 	<?php for($i=0; $i < count($angka); $i++) {  ?>
 	<div class="kotak"><?php echo $angka[$i]; ?></div>
 	<?php } ?>

 	<div class="clear"></div>
 	<br><br><br><br><br>
 	<p>Menampmilkan dengan foreeach</p>
 	<?php foreach ($angka as $key ) : ?>
 		<div class="kotak"><?php echo $key; ?></div>
 	<?php endforeach; ?>
 	
 	
 	<div class="clear"></div>
 	

 	</div>

 </body>
 </html>

