<?php 
/*
Array
Array : Variabel yang menampung banyak nilai
Array : Pasangan antara key & value, yang di mulai dari index nya 0

Ada 2 Penulisan Array
1. Cara Lama = 
$hari = array("Senin","Selasa","Rabu");
2. Cara Baru
$bulan = ["Januari","Februari","Maret"];

Isi elemen pada araray boleh menggunakan tipe data yang berbeda

Ada 2 Cara menampilkan Array
1. var_dump(); ---> menampilkan isi dari sebuah variabel
2. print_r();
3. echo $hari[2];

*/
// 1. Cara Lama
$hari  = array("Senin","Selasa","Rabu");
$books = array('warna' => 'merah', 'bentuk' => 'kotak');

// 1. Cara Baru
$bulan = ["Januari","Februari","Maret"];
$arr   = ['string',123,false];

// cetak 1 element
echo $hari['0'];
echo $hari['2'];



echo "<br>";
echo $books['warna'];
echo $books['bentuk'];




$hari[] = "Kamis";
$hari[] = "Jum;at";

// var_dump($hari);
 ?>