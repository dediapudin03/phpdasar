<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Latihan Array</title>
	<style type="text/css">
		.kotak{
			width: 50px;
			height: 50px;
			background-color: blue;
			text-align: center;
			line-height: 50px;
			margin:3px;
			float: left;
			transition: 1s;
			border-radius: 10%;
			color: white;
		}
		.kotak:hover{
			transform: rotate(360deg);
		}
		.clear{
			clear: both;
		}
	</style>
</head>
<body>
	<?php 
	$angka=[
			[1,2,3],
			[4,5,6],
			[7,8,9]
			];
	// array multi dimensi
	/*echo $angka[2][2];*/
	 ?>
	 <?php foreach ($angka as $key) : ?>
	<div class="kotak">
		<?php foreach ($key as $k ) {
			echo $k;
		} ?>
	</div>
	<div class="clear"></div>
	 <?php endforeach; ?>
	 
	
	  <?php foreach ($angka as $key) : ?>
		
		<?php foreach ($key as $k ) { ?>
			<div class="kotak"><?= $k; ?></div>
		<?php	} ?>
		 <div class="clear"></div>
		<?php endforeach; ?>
	
</body>
</html>