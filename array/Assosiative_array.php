
<?php
// Array Assosisative
// Key nya adalah string yang dibuat sendiri
// Definisi nya sama seperti array 
$mahasiswa = [
			["nama" => "dedi apudin", 
			"nrp" 	=> "0918230", 
			"mail" 	=> "dedi.ap@gmail.com", 
			"jurusan" => "infomratika",
			"photo"	=> "dedi.png"],
			[
			"nama" => "choki", 
			"nrp" => "0918230", 
			"mail" => "choki@gmail.com", 
			"jurusan" => "Akuntansi",
			"photo"	=> "choki.jpg"]
			
			];


			/*echo $mahasiswa[1]["tugas"][1];*/
			// array assosiative key nya adalah string
			// array terluar adalah array numeric
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>Array Assosiative</h1>
	<p>Array dimana key nya adalah string yang dibuat sendiri key->value</p>
	<?php foreach ($mahasiswa as $mhs) : ?>	
	<ul>
		<li><?= $mhs["nama"]; ?></li>
		<li><?= $mhs["nrp"]; ?></li>
		<li><?= $mhs["mail"]; ?></li>
		<li><?= $mhs["jurusan"]; ?></li>
		<li><?= $mhs["photo"]; ?></li>
	</ul>
	<?php endforeach; ?>	
	<!-- <?php var_dump($mahasiswa); ?> -->
</body>
</html>