<?php 
session_start();

// cek cookie
if (isset($_COOKIE["login"])) {

	if ( $_COOKIE['login'] == 'true') {
		$_SESSION['login'] = true;
	}
	// echo ($_COOKIE["login"]);
}

if (isset($_SESSION["login"])) {
	header("location:index.php");
	exit();
}
require 'functions.php';
// cek jika tombol submit ditekan dengan isset

if (isset($_POST['login'])) {
	

	// tangkap variabel dari inputan
	$username	= $_POST['username'];
	$password	= $_POST['password'];

	// cek username pada database dengan perintah mysqli_query($param1,$param2)

	$result = mysqli_query($link, "SELECT * FROM users WHERE username = '$username' ");

	// cek jika username nya ada
	if (mysqli_num_rows($result) === 1 ) {

		// ambil data dari tabel dengan mysql_fetch_assoc dan simpan dalam variabel $row

		$row =  mysqli_fetch_assoc($result);
		
		if (password_verify($password, $row['password'])) {
					# redirect ke halaman index.php
					// set session, apakah session login di halaman lagi ada apa tidak
					$_SESSION["login"] = true;

					// cek cookie
					if (isset($_POST["remember"])) {
						# buat cookie
						setcookie('login','true',time()+60);
					}

					header("location:index.php");
					exit;	
				}

	}

	$error = true;

}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Halaman login</title>
</head>
<body>
	<h1>Halaman Login</h1>
	<!-- pesan error -->
	<?php if (isset($error)) { ?>
		<p style="font-style: italic; color: red;"> Password anda salah</p>

	<?php } ?>
	<form action="" method="post">
	<ul>
		<li>
			<label for="username">username</label>
			<input type="text" name="username" id="username" placeholder="masukan username">
		</li>
		<li>
			<label for="password">password</label>
			<input type="text" name="password" id="password" placeholder="masukan password">
		</li>
		<li>
			<input type="checkbox" name="remember" id="remember">
			<label for="remember">Remember</label>

		</li>
		<li>
			<button type="submit" name="login"> Submit</button>
		</li>
	</ul>
	</form>
</body>
</html>