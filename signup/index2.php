<?php 
// Koneksi Ke Database ada 4 Parameter : Host,Username,Password, database 
$link = mysqli_connect("localhost","root","","phpdasar");

// ambil data dari tabel members / query data nya ( 2 paramter)
$result = mysqli_query($link, "SELECT * FROM members");

if (!$result) {
	echo mysqli_error($link);
}
/*$members = mysqli_fetch_assoc($result);*/
// ambil data(fetch) member dari object result 
/*while ($members = mysqli_fetch_assoc($result)){
var_dump($members);
}
*/



// ada 4 cara mengambil data
/*
1. mysqli_fetch_row() // mengembalikan array numeric, index nya angka
2. mysqli_fetch_assoc() // mengembalikan array assosiative, index nya string
3. mysqli_fetch_array() // mengembalikan kedua nya angka & string
4. mysqli_fetch_objec()
*/

/*$members = mysqli_fetch_row($result);		var_dump($members);  index nya 0 */
/*$members = mysqli_fetch_assoc($result);	var_dump($members);  index nya string */
/*$members = mysqli_fetch_array($result);	var_dump($members);  keduanya */
/*$members = mysqli_fetch_object($result);  var_dump($members->nama);berupa object */



/*while ($members = mysqli_fetch_assoc($result)) {
	var_dump($members);
}*/

 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Koneksi MySQL</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>Latihan</h1>
	<table border="1" cellpadding="10" cellpadding="0">
		
			
		
		<tr>
			<th>No</th>
			<th>Aksi</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Tlp</th>
			<th>Email</th>
		</tr>
		<?php $i = 1; ?>
		<?php while ($members = mysqli_fetch_assoc($result)): ?>

		<tr>
			<td><?php echo $i; ?></td>
			<td><a href="ubah.php">Ubah</a> | <a href="ubah.php">Hapus</a> </td>
			<td><?= ($members["nama"]) ?></td>
			<td><?= ($members["alamat"]) ?></td>
			<td><?= ($members["tlp"]) ?></td>
			<td><?= ($members["email"]) ?></td>
		</tr>
		<?php $i++; endwhile; 
		
		?>

	</table>
</body>
</html>
