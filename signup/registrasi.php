<?php 

require 'functions.php';

// cek jika tombol submit ditekan

if (isset($_POST['register'])) {

	
	// lakukan insert kedatabase dengan pengkondisian menggunakan function register`
	// penggunaan function di ikuti oleh paramter nama fungsi($parameter) 

	if (register($_POST) > 0 ) {
		
			  echo "<script>alert('register berhasil');</script>";

	}else{
		// tampilkan pesan error
		echo mysqli_error($link);
	}

}

 ?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		
		label{
			display: block;
		}
	</style>
</head>
<body>
	<h1>Halaman Register </h1>
	<form action="" method="post">
	<ul>
		<li>
			<label for="username">username</label>
			<input type="text" name="username" id="username" placeholder="masukan username">
		</li>
		<li>
			<label for="password">password</label>
			<input type="password" name="password" id="password" placeholder="masukan password">
		</li>
		<li>
			<label for="password2">ulang password</label>
			<input type="password" name="password2" id="password2" placeholder="ulangi password">
		</li>
		<li><button type="submit" name="register"> Register</button></li>	
		
	</ul>
</form>
</body>
</html>