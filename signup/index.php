<?php 
session_start();
/*var_dump($_SESSION["login"]);*/
// cek session nya
if (!isset($_SESSION["login"])) {
	header("location:login.php");
	exit();
}
require 'functions.php';

$members = query("SELECT * FROM members");

if (isset($_POST["cari"])) {
	 $members = cari($_POST["keyword"]);

}

// ambil data dari tabel members / query data nya ( 2 paramter)
/*$result = mysqli_query($link, "SELECT * FROM members");*/


/*if (!$result) {
	echo mysqli_error($link);
}*/
/*$members = mysqli_fetch_assoc($result);*/
// ambil data(fetch) member dari object result 
/*while ($members = mysqli_fetch_assoc($result)){
var_dump($members);
}
*/

// ada 4 cara mengambil data
/*
1. mysqli_fetch_row() // mengembalikan array numeric, index nya angka
2. mysqli_fetch_assoc() // mengembalikan array assosiative, index nya string
3. mysqli_fetch_array() // mengembalikan kedua nya angka & string
4. mysqli_fetch_objec()
*/

/*$members = mysqli_fetch_row($result);		var_dump($members);  index nya 0 */
/*$members = mysqli_fetch_assoc($result);	var_dump($members);  index nya string */
/*$members = mysqli_fetch_array($result);	var_dump($members);  keduanya */
/*$members = mysqli_fetch_object($result);  var_dump($members->nama);berupa object */



/*while ($members = mysqli_fetch_assoc($result)) {
	var_dump($members);
}*/



 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Koneksi MySQL</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>Input Data Member</h1>
	<a href="logout.php">Logout</a>
	<form action="" method="post" >
		<input type="text" name="keyword" size="40" autocomplete="of" placeholder="Masukan Keyword" autofocus >
		<button type="submit" name="cari">Cari!</button>
	</form>
	<a href="tambah_modular.php">Tambah Data Member</a><br>
	<table border="1" cellpadding="10" cellpadding="0">
		
			
		
		<tr>
			<th>No</th>
			<th>Aksi</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Tlp</th>
			<th>Email</th>
			<th>Photo</th>
		</tr>
		<?php $i = 1; ?>
		<?php foreach($members as $m): ?>

		<tr>
			<td><?php echo $i; ?></td>
			<td><a href="ubah.php?id=<?= $m["id"] ?>">Ubah</a> | <a href="hapus.php?id=<?= $m["id"] ?>" onclick="return confirm('yakin di hapus');">Hapus</a> </td>
			<td><?= $m["nama"] ?></td>
			<td><?= $m["alamat"] ?></td>
			<td><?= $m["tlp"] ?></td>
			<td><?= $m["email"] ?></td>
			<td><img src="img/<?= $m["photo"] ?>" alt="" width=30px; height=30px;></td>
		</tr>
		<?php $i++; endforeach; 
		
		?>

	</table>
</body>
</html>
