<?php 
// pengulangan para array

// penulisan array lama
$books = array('warna' => 'merah', 'bentuk' => 'kotak');

// penulisan array baru
$colors = ["merah","kuning","hijau"];
$angka	= [2,4,5,12,15,45,64];
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>LATIHAN </title>
	<link rel="stylesheet" href="">
	<style>
		.kotak{
			width: 50px;
			height: 50px;
			background:salmon;
			text-align: center;
			line-height: 50px;
			margin:3px;
			float: left;
		}

	</style>
</head>
<body>
	 <?php foreach ($angka as $key =>$value) { ?>
	 <div class="kotak">
	 	
	 		<?php echo $value; ?>
	 	
	 </div>
	<?php } ?>
	 
	<div class="clear"> </div>


	<?php for ($i = 0 ; $i < count($angka) ; $i++) { ?>
		<div class="kotak">
			<?php echo $angka[$i]; ?>
		</div>
	<?php } ?> 
	
</body>
</html>
