<?php 
//
// array numeric = array yang dimulai dari 0
$mahasiswa = [
				["dedi apudin","019283123", "dedi@yahoo.com", "informatika"],
				["yulia","123123", "dedi@yahoo.com", "informatika"],
				["keenar","019283123", "dedi@yahoo.com", "informatika"]
		];

// array multidimensi = array di dalam array

// array assosisative = array dimana index nya adalah string, dan pasangan key dan value ("nama" => "dedi apudin",)
// index nya bukan lagi angka, tapi string, key dan value


?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>LATIHAN FOREACH LAGI</title>
	<link rel="stylesheet" href="">
</head>
<body>
		<h3>daftar mahasiswa</h3>
		<ul>
			<?php foreach ($mahasiswa as $key ) { ?>
				<li>Nama	: <?php echo $key[0] ?></li>
				<li>NRP		: <?php echo $key[1] ?></li>
				<li>Email 	: <?php echo $key[2] ?></li>
				<li>Jurusan	: <?php echo $key[3] ?></li>
			<?php } ?>
		</ul>

		
</body>
</html>

 