<?php 

/*$mahasiswa = [
				["nama" => "dedi apudin", "NRP" => "019283123", "email" =>"dedi@yahoo.com", "Jurusa" =>"informatika"],
				["nama" => "dedi apudin", "NRP" => "019283123", "email" =>"dedi@yahoo.com", "Jurusa" =>"informatika"],
				["nama" => "dedi apudin", "NRP" => "019283123", "email" =>"dedi@yahoo.com", "Jurusa" =>"informatika"]
		];*/
$mahasiswa = [
			["nama" => "dedi apudin", 
			"nrp" => "0918230", 
			"mail" => "dedi.ap@gmail.com", 
			"jurusan" => "infomratika",
			"photo"	=> "dedi.png"],
			
			[
			"nama" => "choki", 
			"nrp" => "0918230", 
			"mail" => "choki@gmail.com", 
			"jurusan" => "Akuntansi",
			"photo"	=> "choki.jpg"]
			
			];

// array assosiative, key nya adalah string yang kita buat sendiri

// array multi dimensi, array di dalam array
$angka =[[1,2,3],[4,16,5],[9,900,5]];

/*var_dump($mahasiswa);
echo $mahasiswa["nama"];*/ // cetak array menggunakan array assosiative

echo $mahasiswa[1]["mail"];
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	DAFTAR MAHASISWA
	<?php foreach ($mahasiswa as $key ) { ?>		
	
	<ul>
		<li>Nama Mahasiswa	: <?= $key["nama"]; ?></li>
		<li>NRP				: <?= $key["nrp"]; ?></li>
		<li>Email			: <?= $key["mail"]; ?></li>
		<li>Jurusan			: <?= $key["jurusan"]; ?></li>
		<li>Photo			: <img src="img/<?= $key["photo"]; ?>" alt=""></li>
	</ul>
	<?php } ?>
</body>
</html>
