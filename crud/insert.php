<?php 
require 'functions.php';
$members = query("SELECT * FROM members");

 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Insert Ke Database</title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>INSERT KE DATABASE</h1>
	<a href="tambah_modular.php">Tambah Data Member</a>
	<hr>
	<table border="1" cellpadding="10" cellpadding="0">		
		<tr>
			<th>No</th>
			<th>Aksi</th>
			<th>Nama</th>
			<th>Alamat</th>
			<th>Tlp</th>
			<th>Email</th>
		</tr>
		<?php $i = 1; ?>
		<?php foreach($members as $m): ?>

		<tr>
			<td><?php echo $i; ?></td>
			<td><a href="ubah.php">Ubah</a> | <a href="hapus.php">Hapus</a> </td>
			<td><?= $m["nama"] ?></td>
			<td><?= $m["alamat"] ?></td>
			<td><?= $m["tlp"] ?></td>
			<td><?= $m["email"] ?></td>
		</tr>
		<?php $i++; endforeach; 
		
		?>
		
	</table>
</body>
</html>