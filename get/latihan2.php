<?php 
if ( !isset($_GET["nama"]) || !isset($_GET["nama"])     ) {

	header("Location: latihan.php");
	exit;
}

// fungsi isset adalah untuk mengecek apakah parameter itu sudah di buat
 ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
</head>
<body>
	<h1>DETAIL MAHASISWA</h1>
	<ul>
		
		<li>Nama Mahasiswa : <?php echo $_GET["nama"]; ?></li>
		<li>NRP : <?php echo $_GET["nrp"]; ?></li>
		<li>Email : <?php echo $_GET["mail"]; ?></li>
		<li>Jurusan : <?php echo $_GET["jurusan"]; ?></li>
	</ul>
	<a href="latihan.php"> Kembali</a>
</body>
</html>